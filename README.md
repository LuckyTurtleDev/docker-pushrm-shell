pushrm can push a readme file to Dockerhub, Red Hat Quay and Harbor v2.

The original pushrm docker images, does not include a shell. Because of this you can not use it for the gitlab-ci.
So I copy pushrm from the original image to the alpine base image.

For the description of pushrm please checkout the [original readme](https://github.com/christian-korneck/docker-pushrm/blob/master/README.md).

This image is avaibale at gitlab `registry.gitlab.com/lukas1818/docker-pushrm-shell` and dockerhub `lukas1818/pushrm-shell`
